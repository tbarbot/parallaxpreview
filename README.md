# ParallaxPreview

Simple tool to preview [Flame's ParallaxComponent](https://flame-engine.org/docs/components.md#parallax-component)
you can test it [here](http://parallaxpreview.tristanbarbot.fr)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
