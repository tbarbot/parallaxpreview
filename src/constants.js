const ALIGNMENTS = {
  BOTTOMCENTER: 'bottom-center',
  BOTTOMLEFT: 'bottom-left',
  BOTTOMRIGHT: 'bottom-right',
  CENTER: 'center',
  CENTERLEFT: 'center-left',
  CENTERRIGHT: 'center-right',
  TOPCENTER: 'top-center',
  TOPLEFT: 'top-left',
  TOPRIGHT: 'top-right',
}

export default {
  ALIGNMENTS: ALIGNMENTS,
}